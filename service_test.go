package instrum

import (
	"context"
	"sync"
	"testing"

	"github.com/stretchr/testify/require"
)

type unitRecorder struct {
	units []string
	mu    sync.Mutex
}

func (tr *unitRecorder) unit(name string) func(context.Context) error {
	return func(ctx context.Context) error {
		tr.mu.Lock()
		defer tr.mu.Unlock()

		tr.units = append(tr.units, name)

		return nil
	}
}

func TestSimpleService(t *testing.T) {
	tr := &unitRecorder{}

	svc := NewService("service")
	svc.Unit("unit1", tr.unit("unit1"))
	svc.Unit("unit2", tr.unit("unit2"))
	svc.Unit("unit3", tr.unit("unit3"))
	svc.Unit("unit4", tr.unit("unit4"), "unit1", "unit2", "unit3")

	err := svc.Start(context.TODO())
	require.Nil(t, err)

	require.Len(t, tr.units, 4)
	require.Equal(t, tr.units[3], "unit4")
}
