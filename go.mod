module gitlab.com/go-figure/instrum

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	gitlab.com/go-figure/logr v0.0.0-20171109172829-d6063063ab0e
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2
	github.com/zemirco/uid v0.0.0-20160129141151-3763f3c45832
)

go 1.13
