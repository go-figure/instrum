package instrum

type contextKey string

func (k contextKey) String() string { return "instrumentation context value " + string(k) }
