package instrum

import (
	"context"
	"os"
	"os/signal"

	"gitlab.com/go-figure/logr"
)

func CancelSignalContext(ctx context.Context, sig ...os.Signal) (context.Context, context.CancelFunc) {
	ctx, cancel := context.WithCancel(ctx)
	sink := logr.SinkFromContext(ctx)

	go func() {
		signals := make(chan os.Signal, 10)
		signal.Notify(signals, sig...)
		defer signal.Stop(signals)

		select {
		case <-ctx.Done():
		case signal := <-signals:
			sink.Event("received_signal", logr.KV{"signal": signal.String()})
			cancel()
		}
	}()

	return ctx, cancel
}
