package instrum

import (
	"context"
	"sync"
)

var waitGroupContextKey = contextKey("waitGroup")

func WaitGroupContext(ctx context.Context, wg *sync.WaitGroup) context.Context {
	if wg == nil {
		panic("wg is nil")
	}

	return context.WithValue(ctx, waitGroupContextKey, wg)
}

func WaitGroupFromContext(ctx context.Context) *sync.WaitGroup {
	wg, ok := ctx.Value(waitGroupContextKey).(*sync.WaitGroup)
	if ok {
		return wg
	}

	var dwg sync.WaitGroup
	// no wg defined in context, create dummy WaitGroup
	return &dwg
}
