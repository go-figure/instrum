package instrum

import (
	"context"

	"github.com/zemirco/uid"
)

func UIDContext(ctx context.Context, name string, size int) (context.Context, string) {
	if len(name) == 0 {
		panic("name is empty")
	}

	key := contextKey("uid:" + name)
	uid := uid.New(size)
	return context.WithValue(ctx, key, uid), uid
}

func UIDFromContext(ctx context.Context, name string) string {
	key := contextKey("uid:" + name)
	uid, _ := ctx.Value(key).(string)
	return uid
}
